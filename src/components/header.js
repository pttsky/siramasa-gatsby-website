import { Link } from "gatsby"
import PropTypes from "prop-types"
import React from "react"
import { Grid, Row, Col } from "react-styled-flexboxgrid"
import styled from "styled-components"
import Img from "gatsby-image"

import theme from "../theme"

const Header = ({ siteTitle, logo }) => (
  <Wrapper>
    <Grid>
      <Row middle='xs'>
        <Col xs>
          <Link to="/" style={{ display: "inline-block" }}>
            <Img fixed={logo} title={siteTitle} alt={siteTitle} />
          </Link>
        </Col>
        <Col>
          <Link to="/portfolio" style={linkStyle}>
            Portfolio
          </Link>
        </Col>
        <Col>
          <Link to="/contact" style={linkStyle}>
            Contact
          </Link>
        </Col>
      </Row>
    </Grid>
  </Wrapper>
)

Header.propTypes = {
  siteTitle: PropTypes.string,
  logo: PropTypes.object.isRequired
}

Header.defaultProps = {
  siteTitle: ``,
}

export default Header

const Wrapper = styled.header`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  z-index: 2;
`

const linkStyle = {
  color: theme.colors.textColor,
  fontFamily: theme.families.ubuntu,
  fontWeight: 500,
  fontSize: '.875rem'
}