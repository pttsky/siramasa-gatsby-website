import React from "react"
import styled from "styled-components"
import UpworkIcon from "./icons/upwork"

export default function () {
  return (
    <UpworkLink rel="noopener noreferrer" href='https://www.upwork.com/fl/serhiipitetsky' target='_blank'>
      <UpworkIconWrapper>
        <UpworkIcon />
      </UpworkIconWrapper>
      <UpworkIconText>
        <UpworkRating>100%</UpworkRating>
        <UpworkSubtitle>Job Success</UpworkSubtitle>
      </UpworkIconText>
    </UpworkLink>
  )
}

const UpworkIconWrapper = styled.span`
  color: #6cd442;
`

const UpworkRating = styled.span`
  display: inline-block;
  vertical-align: middle;
  font-size: 1rem;
  font-weight: 600;
  color: #222;
`

const UpworkSubtitle = styled.span`
  display: block;
  margin-top: .125rem;
  padding-top: .1875rem;
  font-size: .75rem;
  font-weight: 500;
  color: #656565;
  border-top: 4px solid #14BFF4;
`

const UpworkLink = styled.a`
  display: inline-flex;
  align-items: center;
  margin-top: 2rem;
  color: ${({ theme }) => theme.colors.textColor};
`

const UpworkIconText = styled.div`
  margin-left: .75rem;
`