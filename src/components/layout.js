/**
 * Layout component that queries for data
 * with Gatsby's StaticQuery component
 *
 * See: https://www.gatsbyjs.org/docs/static-query/
 */

import React from "react"
import PropTypes from "prop-types"
import { StaticQuery, graphql } from "gatsby"
import { ThemeProvider } from "styled-components"
import { Grid, Row, Col } from "react-styled-flexboxgrid"
import Img from "gatsby-image"
import styled from "styled-components"

import Header from "./header"
import GlobalStyle from "./global-style"
import theme from "../theme"

const Layout = ({ children, title, afterTitle }) => (
  <StaticQuery
    query={graphql`
      query {
        site {
          siteMetadata {
            title
          }
        }
        file(relativePath: { eq: "siramasa.jpg" }) {
          childImageSharp {
            fixed(width: 100) {
              ...GatsbyImageSharpFixed
            }
          }
        }
      }
    `}
    render={data => (
      <ThemeProvider theme={theme}>
        <>
          <Header siteTitle={data.site.siteMetadata.title} logo={data.file.childImageSharp.fixed} />
          <GlobalStyle />
          <Fullpage>
            <main>
              <Hero>
                <Grid>
                  <Row>
                    <Col xs={12} md={6} mdOffset={3}>
                      <H1>{title}</H1>
                      {afterTitle}
                    </Col>
                  </Row>
                </Grid>
              </Hero>
              {children}
            </main>
            <Footer>
              <Grid>
                <Row center='xs'>
                  <Col>
                    <Img fixed={data.file.childImageSharp.fixed}/>
                  </Col>
                </Row>
                © {new Date().getFullYear()}, Built with
                {` `}
                <a href="https://www.gatsbyjs.org">Gatsby</a>
              </Grid>
            </Footer>
          </Fullpage>
        </>
      </ThemeProvider>
    )}
  />
)

Layout.propTypes = {
  afterTitle: PropTypes.element,
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
}

export default Layout

const Footer = styled.div`
  background: white;
  padding-top: 3rem;
  padding-bottom: 4rem;
`

const Fullpage = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;
  padding-top: ${({ theme }) => theme.rems(106 + 16)};
  ${Footer} {
    margin-top: auto;
  }
`

const Hero = styled.div`
  margin-top: 8rem;
  margin-bottom: 8rem;
`

const H1 = styled.h1`
  font-size: 3.5rem;
  font-weight: 400;
  margin-bottom: 2rem;
  font-family: ${({ theme }) => theme.families.ubuntu};
`