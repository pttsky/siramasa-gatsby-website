import { createGlobalStyle } from 'styled-components' 
import theme from '../theme'

export default createGlobalStyle`
  @import url('https://fonts.googleapis.com/css?family=Ubuntu:400,500,700&display=swap');

  * { 
    margin: 0;
    padding: 0;
    box-sizing: border-box;
  }

  html {
    font-size: ${theme.remBase}px;
  }

  body {
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
    font-family: ${theme.families.avenir};
    overflow: hidden;
    overflow-y: scroll;
  }

  a {
    text-decoration: none;
  }

  canvas {
    position: fixed;
    left: 0;
    top: 0;
    z-index: -1;
  }
`