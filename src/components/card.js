import React from "react"
import styled from "styled-components"
import PropTypes from "prop-types"

function Card (props) {
  return (
    <Wrapper>
      <Title>{props.title}</Title>
    </Wrapper>
  )
}

Card.propTypes = {
  title: PropTypes.string.isRequired
}

export default Card

const Wrapper = styled.article`
  width: 100%;
  background: white;
  padding: 2rem;
`

const Title = styled.h3`
  font-family: ${({ theme }) => theme.families.ubuntu};
  font-size: 1.7rem;
  font-weight: 400;
  margin-bottom: 2rem;
`