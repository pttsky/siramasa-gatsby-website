/**
 * entry.js
 *
 * This is the first file loaded. It sets up the Renderer,
 * Scene and Camera. It also starts the render loop and
 * handles window resizes.
 *
 */

import { WebGLRenderer, PerspectiveCamera, Scene, Vector3 } from 'three';
import WaveScene from './scene';

function animation () {
  const scene = new Scene();
  const camera = new PerspectiveCamera();
  const renderer = new WebGLRenderer({ antialias: true });
  const waveScene = new WaveScene();

  // scene
  scene.add(waveScene);

  // camera
  camera.position.set(3, 5, 12);
  camera.lookAt(new Vector3(0,0,5));

  // renderer
  renderer.setPixelRatio(window.devicePixelRatio);
  renderer.setClearColor(0xffffff, 1);

  // render loop
  const onAnimationFrameHandler = (timeStamp) => {
    renderer.render(scene, camera);
    waveScene.update && waveScene.update(timeStamp);
    window.requestAnimationFrame(onAnimationFrameHandler);
  }
  window.requestAnimationFrame(onAnimationFrameHandler);

  // resize
  const windowResizeHanlder = () => {
    const { innerHeight, innerWidth } = window;
    renderer.setSize(innerWidth, innerHeight);
    camera.aspect = innerWidth / innerHeight;
    camera.updateProjectionMatrix();
  };
  windowResizeHanlder();
  window.addEventListener('resize', windowResizeHanlder);

  // dom
  document.body.appendChild( renderer.domElement );
}

export default animation
