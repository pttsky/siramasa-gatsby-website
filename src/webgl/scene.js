import { Group } from 'three'
import Wave from './wave'
import Turbo from './turbo'

export default class SeedScene extends Group {
  constructor() {
    super()

    this.waves = []

    for (let i = 0; i < 20; i++) {
      const wave = new Wave({ freq: 2 + 4 *  Math.random(), speed: .0005 + .0005 * (1 -  Math.random()) })
      const leftRight = Math.sign(i % 2 - .5)
      wave.position.x = 1.2 * leftRight + ( Math.random() - .5)
      wave.position.y = 0
      wave.position.z = .5 * i
      this.waves.push( wave )
    }

    this.add(...this.waves)

    this.turbo = new Turbo()

    let x1 = 0
    let y1 = 0
    document.body.addEventListener('mousemove', (e) => {
      const x2 = e.x / window.innerWidth
      const y2 = e.y / window.innerHeight
      const d = (x1 - x2) ** 2 + (y1 - y2) ** 2
      if (this.turbo.targetValue < 2.5) {
        this.turbo.targetValue += 4 * d
      }
      x1 = x2
      y1 = y2
    })

    let s1 = 0
    window.addEventListener('scroll', () => {
      const s2 = window.scrollY || document.documentElement.scrollTop
      const d = Math.abs(s1 - s2) * .05
      if (this.turbo.targetValue + d < 2) {
        this.turbo.targetValue += d
      }
      s1 = s2
    })
  }

  update(timeStamp) {
    this.turbo.animate()
    this.waves.forEach((wave) => {
      wave.turbo = this.turbo.value
      wave.animate(timeStamp)
    })
  }
}
