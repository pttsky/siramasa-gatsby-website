export default class {
  constructor() {
    this.value = 1
    this.targetValue = 1
  }

  animate() {
    this.value += .2 * (this.targetValue - this.value)
    this.targetValue *= .995
    if (this.targetValue <= 1) {
      this.targetValue = 1
    }
  }

  flame() {
    this.targetValue += .05
  }
}
