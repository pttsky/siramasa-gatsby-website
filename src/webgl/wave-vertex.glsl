varying vec2 vUv;

void main() {
  vUv.xy = position.xy;

  gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
}
