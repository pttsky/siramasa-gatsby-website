precision mediump float;

varying vec2 vUv;

void main() {
   float y = vUv.y;
   float invertY = 1.0 - y;
   float invertSqr = .75 + .25 * invertY * invertY;
   gl_FragColor = vec4( invertSqr, invertSqr, invertSqr, 1.0 );
}
