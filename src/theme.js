import { css } from 'styled-components' 

const breakpoints = {
  xs: 0,
  sm: 40,
  md: 64,
  lg: 76
}

const media = Object.keys(breakpoints).reduce((acc, label) => {
  acc[label] = (...args) => css`
    @media (min-width: ${breakpoints[label]}em) {
      ${css(...args)}
    }
  `
  return acc
}, {})

const remBase = 16

export default {
  colors: {
    textColor: 'hsla(0, 0%, 28%, 1)'
  },
  families: {
    avenir: 'Avenir, Helvetica, Arial, sans-serif',
    ubuntu: 'Ubuntu, sans-serif'
  },
  flexboxgrid: {
    breakpoints,
    container: {
      sm: 39,
      md: 63,
      lg: 75
    },
    gutterWidth: 2,
    outerMargin: 1
  },
  media,
  remBase,
  rems: px => `${px / remBase}rem`
}