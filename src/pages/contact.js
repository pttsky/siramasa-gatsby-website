import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const ContactPage = () => (
  <Layout title="Contact">
    <SEO title="Contact" />
  </Layout>
)

export default ContactPage
