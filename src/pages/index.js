import React from "react"
import { Grid, Row, Col } from "react-styled-flexboxgrid"
import styled from "styled-components"

import UpworkLink from "../components/upwork-link"
import Layout from "../components/layout"
import SEO from "../components/seo"
import Card from "../components/card"

const IndexPage = () => (
  <Layout title='We are SiraMasa' afterTitle={
    <>
      <p>Team of web masters. We do websites, small to large scale web apps and PWAs. 
        We use React, Redux, Gatsby, Next.js, THREE.js, Node.js., MongoDB and many more</p>
      <UpworkLinkSpacing>
        <UpworkLink />
      </UpworkLinkSpacing>
    </>
  }>
    <SEO title="Welcome" />
    <Grid>
      <Section>
        <H2>Our services</H2>
        <Row>
          <Col xs={12} md={4}>
            <Card title="Websites and animation" />
          </Col>
          <Col xs={12} md={4}>
            <Card title="Web apps" />
          </Col>
          <Col xs={12} md={4}>
            <Card title="PWAs" />
          </Col>
        </Row>
      </Section>
      <Section>
        <H2>Portfolio</H2>
      </Section>
    </Grid>
  </Layout>
)

export default IndexPage

const Section = styled.section`
  padding-top: 8rem;
  padding-bottom: 8rem;
`

const H2 = styled.h2`
  font-family: ${({ theme }) => theme.families.ubuntu};
  font-size: 2.5rem;
  font-weight: 400;
  margin-bottom: 2rem;
`

const UpworkLinkSpacing = styled.div`
  margin-top: 3rem;
`
