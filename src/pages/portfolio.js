import React from "react"

import Layout from "../components/layout"
import SEO from "../components/seo"

const PortfolioPage = () => (
  <Layout title="Portfolio">
    <SEO title="Portfolio" />
  </Layout>
)

export default PortfolioPage
